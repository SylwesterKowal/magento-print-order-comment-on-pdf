<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PrintOrderCommentOnPdf\Rewrite\Fooman\PrintOrderPdf\Model\Pdf;

class Order extends \Fooman\PrintOrderPdf\Model\Pdf\Order
{
    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private $appEmulation;
    private $groupRepository;
    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation $appEmulation,
        array $data = [],
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository
    ) {
        $this->appEmulation = $appEmulation;
        $this->groupRepository = $groupRepository;
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $appEmulation,
            $data
        );
    }

    public function getGroupName($groupId){
        $group = $this->groupRepository->getById($groupId);
        return $group->getCode();
    }

    /**
     * @param \Magento\Framework\DataObject $item
     * @param \Zend_Pdf_Page $page
     * @param \Magento\Sales\Model\Order $order
     * @param $lp_
     * @return \Zend_Pdf_Page
     */
    protected function _drawItem(
        \Magento\Framework\DataObject $item,
        \Zend_Pdf_Page $page,
        \Magento\Sales\Model\Order $order,
        $lp_ = ''
    ) {
        $type = $item->getOrderItem()->getProductType();
        $renderer = $this->_getRenderer($type);
        $renderer->setOrder($order);
        $renderer->setItem($item);
        $renderer->setPdf($this);
        $renderer->setPage($page);
        $renderer->setRenderedModel($this);

        $renderer->draw($lp_);

        return $renderer->getPage();
    }

    /**
     * Return PDF document
     *
     * @param  \Magento\Sales\Model\Order[] $orders
     *
     * @return \Zend_Pdf
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Pdf_Exception
     */
    public function getPdf($orders = [])
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('order');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);

        foreach ($orders as $order) {
            if ($order->getStoreId()) {
                $this->appEmulation->startEnvironmentEmulation(
                    $order->getStoreId(),
                    \Magento\Framework\App\Area::AREA_FRONTEND,
                    true
                );
            }
            $page = $this->newPage();

            $order->setOrder($order);
            /* Add image */
            $this->insertLogo($page, $order->getStore());
            /* Add address */
            $this->insertAddress($page, $order->getStore());
            /* Add head */
            $this->insertOrder(
                $page,
                $order,
                $this->_scopeConfig->isSetFlag(
                    self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $order->getStoreId()
                )
            );
            $page->drawText(
                __('Grupa: ') . $this->getGroupName($order->getCustomerGroupId()), 260, 755, 'UTF-8');

            $page->drawText(
                __('E-mail: ') . $order->getCustomerEmail(), 400, 755, 'UTF-8');


            /* Add table */
            $this->_drawHeader($page);
            /* Add body */
//            $lp = 1;
//            foreach ($order->getAllItems() as $item) {
//                if ($item->getParentItem()) {
//                    continue;
//                }
//
//                /* Keep it compatible with the invoice */
//                $item->setQty($item->getQtyOrdered());
//                $item->setOrderItem($item);
//
//                $this->_setFontRegular($page, 4);
//                $this->_setFontBold($page, 4);
//                /* Draw item */
//                $this->_drawItem($item, $page, $order, $lp);
//                $page = end($pdf->pages);
//                $lp++;
//            }



            $itemsByStstus = [];

            foreach ($order->getAllItems() as $item) {
                $itemsByStstus[$this->_getItemStatus($item)][] = $item;
            }

            $lp = 1;
            foreach ($itemsByStstus['Invoiced'] as $item) {
                if ($item->getParentItem()) {
                    continue;
                }

                /* Keep it compatible with the invoice */
                $item->setQty($item->getQtyOrdered());
                $item->setOrderItem($item);

                /* Draw item */
                $this->_drawItem($item, $page, $order, $lp );
                $page = end($pdf->pages);
                $lp++;
            }


            foreach ($itemsByStstus['Partial'] as $item) {
                if ($item->getParentItem()) {
                    continue;
                }

                /* Keep it compatible with the invoice */
                $item->setQty($item->getQtyInvoiced());
                $item->setOrderItem($item);

                /* Draw item */
                $this->_drawItem($item, $page, $order, $lp );
                $page = end($pdf->pages);
                $lp++;
            }

            /* Add table */
            $this->_drawHeader($page);
            $page->drawText(__('BACKORDER'), 140, $this->y+25, 'UTF-8');

            $lp = 1;

            foreach ($itemsByStstus['Partial'] as $item) {
                if ($item->getParentItem()) {
                    continue;
                }

                /* Keep it compatible with the invoice */
                $item->setQty($item->getQtyOrdered() - $item->getQtyInvoiced());
                $item->setOrderItem($item);

                /* Draw item */
                $this->_drawItem($item, $page, $order, $lp .'.B');
                $page = end($pdf->pages);
                $lp++;
            }

            foreach ($itemsByStstus['Ordered'] as $item) {
                if ($item->getParentItem()) {
                    continue;
                }

                /* Keep it compatible with the invoice */
                $item->setQty($item->getQtyOrdered());
                $item->setOrderItem($item);

                /* Draw item */
                $this->_drawItem($item, $page, $order, $lp . '.B');
                $page = end($pdf->pages);
                $lp++;
            }


            /* Add totals */
            $this->insertTotals($page, $order);
            if ($order->getStoreId()) {
                $this->appEmulation->stopEnvironmentEmulation();
            }

            foreach ($order->getStatusHistoryCollection() as $status){
                if(is_null($status->getComment())) continue;
                $textChunk = wordwrap(strip_tags((string)$status->getComment()), 120, "\n");
                foreach(explode("\n", $textChunk) as $textLine){
                    if ($textLine!=='') {
                        $page->drawText(strip_tags(ltrim($textLine)), 35, $this->y, 'UTF-8');
                        $this->y -= 15;
                    }
                }
            }

        }
        $this->_afterGetPdf();
        return $pdf;
    }

    private function _getItemStatus($item)
    {
        $status = 'Ordered';
        $ordered = $item->getQtyOrdered();
        $invoiced = $item->getQtyInvoiced();
        if ($ordered == $invoiced) {
            $status = 'Invoiced';
        }
        if ($ordered > $invoiced && $invoiced > 0) {
            $status = 'Partial';
        }
        return $status;
    }
}

